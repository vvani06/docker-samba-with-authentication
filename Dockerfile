FROM alpine:3.8
 
RUN apk update
RUN apk add tzdata && cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
RUN apk add samba
 
COPY conf/smb.conf /etc/samba/smb.conf
COPY conf/samba_users.txt .
 
RUN mkdir /share/smb -p
RUN chmod 0777 /share/smb

RUN cat samba_users.txt | awk '{print "adduser -D ", $1}' | /bin/sh
RUN cat samba_users.txt | awk '{print "(echo ",$2,"; echo ",$2,") | pdbedit -a ",$1, "-t" }' | /bin/sh
 
RUN rm samba_users.txt
 
ENTRYPOINT smbd -S -F --no-process-group </dev/null
 
EXPOSE 445